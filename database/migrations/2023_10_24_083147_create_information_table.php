<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('information', function (Blueprint $table) {
            $table->id();
            $table->text("image_1")->nullable();
            $table->text("image_2")->nullable();
            $table->text("image_3")->nullable();
            $table->text("image_4")->nullable();
            $table->text("image_5")->nullable();
            $table->string('email')->nullable();
            $table->string('model')->nullable();
            $table->string('make')->nullable();
            $table->integer('year')->nullable();
            $table->tinyText('location')->nullable();
            $table->json('build_list')->nullable();
            $table->json('wish_list')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('information');
    }
};
