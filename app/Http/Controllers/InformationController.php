<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Information;


class InformationController extends Controller
{
    //
    
    public function index(Request $request) 
    {
        $Informations = Information::get();

        $data = [];
        foreach ($Informations as $key => $value) 
        {
            $data [] = array(
                            'id'           => $value->id,
                            'image_1'      => asset('storage/images/'.$value->image_1),
                            'image_2'      => asset('storage/images/'.$value->image_2),
                            'image_3'      => asset('storage/images/'.$value->image_3),
                            'image_4'      => asset('storage/images/'.$value->image_4),
                            'image_5'      => asset('storage/images/'.$value->image_5),
                            'email'        => $value->email,
                            'model'        => $value->model,
                            'trim'         => $value->make,
                            'year'         => $value->year,
                            'zip'          => $value->location,
                            'build_list'   => $value->build_list,
                            'wish_list'    => $value->make,
                        );
        }


        return response()->json(['success' => true, 'data' => $data]);
    }




    public function searchByEmail(Request $request) 
    {
        $Information = Information::where('email', $request->email)->orderBy('id', 'desc')->first();
      
   
            $data [] = array(
                            'id'           => $Information->id,
                            'image_1'      => asset('storage/images/'.$Information->image_1),
                            'image_2'      => asset('storage/images/'.$Information->image_2),
                            'image_3'      => asset('storage/images/'.$Information->image_3),
                            'image_4'      => asset('storage/images/'.$Information->image_4),
                            'image_5'      => asset('storage/images/'.$Information->image_5),
                            'email'        => $Information->email,
                            'model'        => $Information->model,
                            'trim'         => $Information->make,
                            'year'         => $Information->year,
                            'zip'          => $Information->location,
                            'build_list'   => $Information->build_list,
                            'wish_list'    => $Information->wish_list,
                        );


        return response()->json(['success' => true, 'data' => $data]);
    }



    public function store(Request $request)
    {

        try {

            if($request->updateTheRecord == false)
            {
                $validatedData = $request->validate([
                    'image_1'    => 'required|image|mimes:jpeg,png,jpg|max:2048',
                    'image_2'    => 'required|image|mimes:jpeg,png,jpg|max:2048',
                    'image_3'    => 'required|image|mimes:jpeg,png,jpg|max:2048',
                    'image_4'    => 'required|image|mimes:jpeg,png,jpg|max:2048',
                    'image_5'    => 'required|image|mimes:jpeg,png,jpg|max:2048',
                    'email'      => 'required',
                    'modelcustom'=> 'required',
                    'trimcustom' => 'required',
                    'zipcustom'  => 'required',
                    'yearcustom' => 'required',
                    'build_list' => 'required',
                    'wish_list'  => 'required',
                ]);
            }else{
                $validatedData = $request->validate([                   
                    'email'      => 'required',
                    'modelcustom'=> 'required',
                    'trimcustom' => 'required',
                    'zipcustom'  => 'required',
                    'yearcustom' => 'required',
                    'build_list' => 'required',
                    'wish_list'  => 'required',
                ]);
            }
            


            if ($request->hasFile('image_1')) {
                $image_1 = $request->file('image_1');  
                $newName = time() . '_' . $image_1->getClientOriginalName();
                $image_1->storeAs('public/images', $newName);
                $image_1 = $newName;    
            }
    
            if ($request->hasFile('image_2')) {
                $image_2 = $request->file('image_2');  
                $newName = time() . '_' . $image_2->getClientOriginalName();
                $image_2->storeAs('public/images', $newName);   
                $image_2 = $newName;    
            }
    
            if ($request->hasFile('image_3')) {
                $image_3 = $request->file('image_3');  
                $newName = time() . '_' . $image_3->getClientOriginalName();
                $image_3->storeAs('public/images', $newName);       
                $image_3 = $newName;
            }
    
    
            if ($request->hasFile('image_4')) {
                $image_4 = $request->file('image_4');  
                $newName = time() . '_' . $image_4->getClientOriginalName();
                $image_4->storeAs('public/images', $newName); 
                $image_4 = $newName;      
            }


            if ($request->hasFile('image_5')) {
                $image_5 = $request->file('image_5');  
                $newName = time() . '_' . $image_5->getClientOriginalName();
                $image_5->storeAs('public/images', $newName); 
                $image_5 = $newName;      
            }
            
            $email      = $request->email;
            $make       = $request->trimcustom;  
            $model      = $request->modelcustom;
            $year       = $request->yearcustom;
            $location   = $request->zipcustom;
            $build_list = $request->build_list;
            $wish_list  = $request->wish_list;
    
            if($request->updateTheRecord == false)
            {
                $Information             = new Information();
                $Information->image_1    = $image_1;
                $Information->image_2    = $image_2;
                $Information->image_3    = $image_3;
                $Information->image_4    = $image_4;
                $Information->image_5    = $image_5;
                $Information->email      = $email;
                $Information->make       = $make;
                $Information->model      = $model;
                $Information->year       = $year;
                $Information->location   = $location;
                $Information->build_list = $build_list;
                $Information->wish_list  = $wish_list;
            }else{


                $Information             = Information::where('email', $email)->orderBy('id', 'desc')->first();
                if(isset($image_1) && $image_1 !== '')
                {
                    $Information->image_1    = $image_1;
                }

                if(isset($image_2) && $image_2 !== '')
                {
                    $Information->image_2    = $image_2;
                }

                if(isset($image_3) && $image_3 !== '')
                {
                    $Information->image_3    = $image_3;
                }

                if(isset($image_4) && $image_4 !== '')
                {
                    $Information->image_4    = $image_4;
                }

                if(isset($image_5) && $image_5 !== '')
                {
                    $Information->image_5    = $image_5;
                }
           
                $Information->email      = $email;
                $Information->make       = $make;
                $Information->model      = $model;
                $Information->year       = $year;
                $Information->location   = $location;
                $Information->build_list = $build_list;
                $Information->wish_list  = $wish_list;
            }
           
            
            if($Information->save())
            {
                return response()->json(['success' => true, 'data' => 'Record saved successfully']);
    
            }
        } catch (\Throwable $th) {
            return response()->json(['success' => false, 'data' => $th->getMessage()]);

        }
        


    }
}
